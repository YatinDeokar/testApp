

import 'package:flutter/material.dart';
import 'package:habit_tracker_app/presentation/Widgets/clipping_class.dart';

class MyBezierCurve extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: ClippingClass(),
      child: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xff221b4c),
              Color(0xff151b2b),
            ],
          ),
        ),
      ),
    );
  }
}