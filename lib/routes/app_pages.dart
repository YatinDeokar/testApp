import 'package:get/get.dart';
import 'package:habit_tracker_app/application/home_binding.dart';
import 'package:habit_tracker_app/application/login_binding.dart';
import 'package:habit_tracker_app/presentation/pages/home_page.dart';
import 'package:habit_tracker_app/presentation/pages/login_page.dart';
part 'app_routes.dart';

class AppPages {
  const AppPages._();

  static final routes = [
    GetPage(
        name: _Paths.LOGIN_PAGE,
        page: () => LoginPage(),
        binding: LoginBinding()
    ),
    GetPage(
      name: _Paths.HOME_PAGE,
      page: () =>  HomeScreen(),
      binding: HomeBinding(),
    ),

  ];
}
