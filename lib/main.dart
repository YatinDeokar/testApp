import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:habit_tracker_app/presentation/pages/home_page.dart';
import 'package:habit_tracker_app/routes/app_pages.dart';

import 'presentation/pages/login_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      getPages: AppPages.routes,
      initialRoute: Routes.LOGIN_PAGE,
      defaultTransition: Transition.fade,
    );
  }
}

