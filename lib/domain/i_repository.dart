

import 'package:dartz/dartz.dart';
import 'package:habit_tracker_app/common/category.dart';

abstract class IRepository{

  Future<Either<String, String>> login(String userId, String password);
  Future<Either<String, List<Category>>> fetchBooks();

}